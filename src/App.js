import React,{useReducer} from 'react';
import './App.css';
import { makeStyles } from '@material-ui/core/styles';
import Stepper from '@material-ui/core/Stepper';
import Step from '@material-ui/core/Step';
import Information from './components/Information'
import StepLabel from '@material-ui/core/StepLabel';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Image from './components/Image'
import Confirm from './components/Confirm'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  backButton: {
    marginRight: theme.spacing(1),
  },
  instructions: {
    marginTop: theme.spacing(1),
    marginBottom: theme.spacing(1),
  },
}));

function getSteps(props) {
  return ['Infromation', 'Image', 'Confirm'];
}

function getStepContent(stepIndex) {
  switch (stepIndex) {
    case 0:
      return 'Enter your information';
    case 1:
      return 'Select your avatar';
    case 2:
      return 'Confirm ?';
    default:
      return 'Unknown stepIndex';
  }
}


const reducer = (state , action) => {
    switch (action.type) {
      case 'next':
        return {...state ,activeStep :state.activeStep + 1}
      case 'back' :
        if(state.activeStep==1) {
          return {...state ,activeStep :state.activeStep - 1, information:false}
        }
        else if(state.activeStep==2) {
          return {...state ,activeStep :state.activeStep - 1, image:false}
        }

      case 'reset' :
        return { activeStep : 0,
          information: false,
          imageurl: '',
          data: {
            firstname : "" ,
            lastname : "" ,
            email : "",
            select: {}
          }}
      case 'information' :
        return {...state, data: action.data , information:true}
      case 'changeinformation' :
        return {...state, information:false}
      case 'seturl' :
        return {...state , imageurl: action.file}
      default :
        return state
    }
}

let statesintial = {
  activeStep : 0,
  information: false,
  imageurl: '',
  data: {
    firstname : "" ,
    lastname : "" ,
    email : "",
    select: {}
  }
}
const ids = {
  0:'information',
  1 : 'image',
  2: 'confirm'
}

function App() {


  const classes = useStyles();
  const [states, dispatch] = useReducer(reducer , statesintial)
  const steps = getSteps();

  const handleNext = () => {
    if ((states.activeStep===0) && (states.information) ) {
      dispatch({type:'next'});
    }
    else if((states.activeStep===1) && states.imageurl!='')
    {
      dispatch({type:'next'});
    }
    else if(states.activeStep===2)
    {
      dispatch({type:'next'});
    }
  };

  const handleBack = () => {
    dispatch({type:'back'});
  };

  const handleReset = () => {
    dispatch({type :'reset'});
  };


return (
    <div className={classes.root}>
      <Stepper activeStep={states.activeStep} alternativeLabel>
        {steps.map((label) => (
          <Step key={label}>
            <StepLabel>{label}</StepLabel>
          </Step>
        ))}
      </Stepper>

      {states.activeStep==0 ?
        <Information dispatch= {dispatch} data={states.data}/> 
      : states.activeStep==1 ? 
       <Image dispatch = {dispatch} states={states}/>
      :
      <Confirm states={states}/>
      }
      
      
      <div className='step'>
        {states.activeStep === steps.length ? (
          <div>
            <Typography className={classes.instructions}>All steps completed</Typography>
            <Button onClick={handleReset}>Reset</Button>
          </div>
        ) : (
          <div>
            <Typography className={classes.instructions}>{getStepContent(states.activeStep)}</Typography>
            <div>
              <Button
                disabled={states.activeStep === 0}
                onClick={handleBack}
                className={classes.backButton}
              >
                Back
              </Button>
              <Button form ={ids[0]} type ='sumbmit' variant="contained" color="primary" onClick={handleNext}>
                {states.activeStep === steps.length - 1 ? 'Finish' : 'Next'}
              </Button>
            </div>
          </div>
        )}
      </div>
    </div>
  );
}

export default App;
