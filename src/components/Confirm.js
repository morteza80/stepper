import React from 'react'

export default function Confirm({states}) {
    return (
        <div>
            <form className="form">
                <p>firstname : {states.data.firstname}</p>
                <p>lastname : {states.data.lastname}</p>
                <p>email : {states.data.email}</p>
                <img className="image" src={states.imageurl}/>
                {<p>details : {states.data.Css? <span>css</span> : null} {states.data.React? " , React" : null } {states.data.Html? " , Html" : null} {states.data.Js? " , Js" : null} </p>}
            </form>
        </div>
    )
}
