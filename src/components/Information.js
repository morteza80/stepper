import React from 'react';
import {useForm , Controller} from 'react-hook-form';
import TextField from '@material-ui/core/TextField';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Select from 'react-select';
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";


const selectval = {
    value : yup.string().required() ,
    label : yup.string().required()
}
const options = [
    { value: 'design', label: 'Design' },
    { value: 'uiux', label: 'UI/Ux' },
    { value: 'backend', label: 'Backend' }
  ]
  const schema = yup.object().shape({
    firstname : yup.string("name must be string").required("enter your name").min(5,"enter more"),
    lastname: yup.string("lastname must be string").required("enter your lastname").min(5,"enter more "),
    email: yup.string().email("email is invalid") ,
    select : yup.object().required("select something")
});


export default function Information(props) {
    const { handleSubmit, control } = useForm({
        resolver: yupResolver(schema),
        reValidateMode: "onChange"
     });
    
    return (
            <form id = 'information'   className='form' onSubmit = {handleSubmit( (data) => props.dispatch({type : "information" , data : data}))}>
                <Controller
                name="firstname"
                className='controller'
                control={control}
                defaultValue={props.data.firstname}
                render={({ field: { onChange, value } , fieldState: {error} }) => (
                    <TextField
                    label="First Name"
                    variant="filled"
                    className='textfield'
                    value={value}
                    onChange={onChange}
                    error = {error}
                    helperText = {error? error.message : null}
                    />
                )}
                />
                <Controller
                name="lastname"
                className='controller'
                control={control}
                defaultValue={props.data.lastname}
                render={({ field: { onChange, value }, fieldState: {error} }) => (
                    <TextField
                    label="Last Name"
                    variant="filled"
                    className='textfield'
                    value={value}
                    onChange={onChange}
                    error = {error}
                    helperText = {error? error.message : null}
                    />
                )}
                />
                <Controller
                name = "select"
                control ={control}
                render ={ ({ field : {onChange,value} , fieldState : {error} } ) => {
                    if(!error) {
                        return (
                        <Select 
                        className='select' 
                        options={options} 
                        onChange = {onChange} 
                        value ={value} 
                        error = {error}
                        />
                        )
                    }
                    else {
                        return (
                        <div className='erselect'>
                            <Select 
                            className='select' 
                            options={options} 
                            onChange = {onChange} 
                            value ={value} 
                            error = {error}
                            />
                            <h4> {error.message} </h4>
                        </div>
                        )
                    }
                }

                }
                />

                <FormGroup row >
                    <Controller 
                    name="React" 
                    control ={control}
                    render = { ({field: { onChange, value } }) => (
                        <FormControlLabel
                            control={<Checkbox  name="checkedA" onChange={onChange} value = {value} />}
                            label="React"
                        />
                    )}
                    />
                    <Controller 
                    name="Css" 
                    control ={control}
                    render = { ({field: { onChange, value } }) => (
                        <FormControlLabel
                            control={<Checkbox  name="checkedA" onChange={onChange} value = {value}/>}
                            label="Css"

                        />
                        )}
                    />
                    <Controller 
                        name="Html" 
                        control ={control}
                        render = { ({field: { onChange, value } }) => (
                        <FormControlLabel
                            control={<Checkbox  name="checkedA" onChange={onChange} value = {value}/>}
                            label="Html"

                        />
                        )}
                    />
                    <Controller 
                    name="Js" 
                    control ={control}
                    render = { ({field: { onChange, value } }) => (
                        <FormControlLabel
                            control={<Checkbox  name="checkedA" onChange={onChange} value = {value}/>}
                            label="Js"

                        />
                        )}
                    />
                </FormGroup>
                
                
                
                



                <Controller
                name="email"
                className='controller'
                control={control}
                defaultValue={props.data.email}
                render={({ field: { onChange, value } , fieldState: {error}}) => (
                    <TextField
                    label="Email"
                    className='textfield'
                    variant="filled"
                    value={value}
                    onChange={onChange}
                    error = {error}
                    helperText = {error? error.message : null}
                    />
                )}
                />



            </form>
       
    )
}
