import React from 'react'
import {useForm} from "react-hook-form";
import { yupResolver } from '@hookform/resolvers/yup';
import * as yup from "yup";

const schema = yup.object().shape({
    input2: yup.mixed().required("select something")
})

export default function Image(props) {

    const handleChange = (event) => {
        props.dispatch({
            type: 'seturl',
            file: URL.createObjectURL(event.target.files[0])
        })
    }
    return (
        <div>
            <form id= 'image' className='form' >
                <input type="file" onChange = {(e) => handleChange(e)} />
                <img className="image" src={props.states.imageurl}/>
            </form>
        </div>
    )
}
